// import the user model in our controllers. So that our controllers or controller function may have access to our user model.
const Product = require("../models/Product");
// const User = require("../models/User");
const bcrypt = require('bcryptjs');



//----------------------Create Product--------------------------

module.exports.registerProduct = (req, res) => {
	console.log(req.body);

	let newProduct = new Product({
		productCode: req.body.productCode,
		productName: req.body.productName,
		stampCaseColor: req.body.stampCaseColor,
		preInkedColor: req.body.preInkedColor,
		size: req.body.size,
		price: req.body.price,
	});

	newProduct.save()
	.then(product => res.send(product))
	.catch(error => res.send(error));
};

//--------------------RETRIEVAL OF ALL Products-----------------
module.exports.getAllProducts = (req, res) => {
	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

//-------SECTION: GETTING SINGLE PRODUCT--------------------------

module.exports.getSingleProduct = (req, res) =>{
	console.log(req.params);

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//--------------SECTION: Updating a Product--------------------------------
module.exports.updateProduct = (req, res) =>{
	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error))
}



//--------------SECTION: archiving a Product--------------------------------
module.exports.archiveProduct = (req, res) =>{
	console.log(req.user.id); //id of the logged in user
	console.log(req.params.id); //id of the product to be updated

	let updates = {
		isActive: false
	}


	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error))

};

//--------------SECTION: activate a Product--------------------------------
module.exports.activateProduct = (req, res) =>{
	console.log(req.user.id); //id of the logged in user
	console.log(req.params.id); //id of the product to be updated

	let updates = {
		isActive: true
	}


	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error))

};


//--------------SECTION: Get Active Products--------------------------------


module.exports.getActiveProducts = (req, res) => {

	console.log(`showing active products`)


	Product.find({isActive: true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(error => res.send(error))

};



//-----------------GET ALL ORDERS -------------------------------------------

module.exports.getAllOrders = (req, res) =>{

	Product.find({})
	.then(result => res.send(result.orders))
	.catch(error => res.send(error))




};
