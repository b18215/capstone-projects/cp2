// import the user model in our controllers. So that our controllers or controller function may have access to our user model.
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require('bcryptjs')

const auth = require("../auth")

//----------------------Create user---------------------------

module.exports.registerUser = (req, res) => {
	console.log(req.body);


	/*
		bcrypt.hashSync(<stringToBeHashed>,<saltRounds>)

		salt rounds - number of times the characters in the hash randomized
	*/

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		houseNumberAndStreet: req.body.houseNumberAndStreet,
		barangayAddr: req.body.barangayAddr,
		cityAddr: req.body.cityAddr,
		provinceAddr: req.body.provinceAddr,
		regionAddr: req.body.regionAddr,
		mobileNumber: req.body.mobileNumber,
		password: hashedPW

	});

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error));
};

//--------------------RETRIEVAL OF ALL USERS-----------------
module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

//---------SECTION: Login User------------------------------
module.exports.loginUser = (req, res) => {
	console.log(req.body)

/*
	1. find the user by the email
	2. If we found user, we will check the password
	3. if we don't find the user, then we will send a message to the Client.
	4. If upon checking the found user's password is the same as our input password, we will generate the 'token / key' to access our app. If not, we will turn them away by sending a message to the Client.
*/


	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send(false)
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPasswordCorrect);

			/*
				compareSync()
				will return a boolean value
				so if it matches, this will return true. 
				If not this will return false.
			*/

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send(false)
			}
		}
	})
	.catch(error => res.send(error));

};


//-------SECTION: GETTING SINGLE USER DETAILS--------------------------

module.exports.getUserDetails = (req, res) =>{
	console.log(req.user);

	/* expected output:
		{	
			id: token
			email: test@email.com
			isAdmin: false
		}
	*/
	// find the logged in user's document from our Database and send it to the Client by it's id.

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


//-------SECTION: GETTING EMAIL --------------------------


module.exports.checkEmailExists = (req, res) => {
	console.log(req.body)


	User.findOne({email: req.body.email})
	.then(result => {

		if(result === null){
			return res.send(false)
		} else {
				return res.send(true)
		} 
	})
	.catch(error => res.send(error));

};


//-------SECTION: UPDATING USER DETAILS --------------------------

module.exports.updateUserDetails = (req, res) =>{
	console.log(req.body); //input for new values
	console.log(req.user.id); //check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNumber: req.body.mobileNumber,
		houseNumberAndStreet: req.body.houseNumberAndStreet,
		barangayAddr: req.body.barangayAddr,
		cityAddr: req.body.cityAddr,
		provinceAddr: req.body.provinceAddr,
		regionAddr: req.body.regionAddr
	}


	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))

};


//-------SECTION: UPDATING PASSWORD ONLY --------------------------

module.exports.updateUserPassword = (req, res) =>{
	console.log(req.body); //input for new values
	console.log(req.user.id); //check the logged in user's id

	const hashedPW = bcrypt.hashSync(req.body.password, 10);


	let updates = {
		password: hashedPW
	}


	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))

};
//-------SECTION: UPDATE AN ADMIN --------------------------

module.exports.updateAdmin = (req, res) =>{
	console.log(req.user.id); //id of the logged in user
	console.log(req.params.id); //id of the user to be updated

	let updates = {
		isAdmin: true
	}


	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))

};


//-------SECTION: CREATE ORDER --------------------------

module.exports.createOrder = async (req, res) =>{
	console.log(req.user.id); //id of the logged in user (with token)
	console.log(req.params.productCode) // the productCode from our request params.
	
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	let isUserPurchased = await User.findById(req.user.id).then(user =>{
		console.log(user);


		let newPurchase = {
			productCode: req.params.productCode,
			price: req.body.price,
			status: req.body.status
		}

		user.purchase.push(newPurchase)
		return user.save()
		.then(user => true)
		.catch(error => error.message)
	})

	

		if(isUserPurchased !== true){
		return res.send({message: isUserPurchased})
		}

		let isPurchaseUpdated = await Product.findOne({productCode: req.params.productCode}).then (product =>{
			let order = {
				userId: req.user.id
			}
			product.orders.push(order)

			return product.save()
			.then(product => true)
			.catch(error => error.message)
		})

		if(isPurchaseUpdated !== true){
			return res.send({message: isPurchaseUpdated})
		}

		if(isUserPurchased && isPurchaseUpdated){
			return res.send({message: 'Item has been added to cart'})
		}

};


/*//-------SECTION: UPDATING USER ORDER DETAILS --------------------------


module.exports.updateUserOrderStatus = async (req, res) =>{
	console.log(req.user.id); //id of the logged in user (with token)
	console.log(req.params.productId) // the productCode from our request params.
	
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	let isUserPurchased = await User.findById(req.user.id).then(user =>{
		console.log(user);


		let newPurchase = {
			status: req.body.status
		}


		User.findByIdAndUpdate(req.params.productId, newPurchase, {new: true})
		.then(updatedUser => res.send(updatedUser))
		.catch(error => res.send(error))

	})

	

		if(isUserPurchased !== true){
		return res.send({message: isUserPurchased})
		}

		let isPurchaseUpdated = await Product.findOne({productId: req.params.productId}).then (product =>{
			let order = {
				status: req.user.id
			}

			Product.findByIdAndUpdate(req.params.productId, order, {new: true})
			.then(updatedUser => res.send(updatedUser))
			.catch(error => res.send(error))
		})

		if(isPurchaseUpdated !== true){
			return res.send({message: isPurchaseUpdated})
		}

		if(isUserPurchased && isPurchaseUpdated){
			return res.send({message: 'Status updated to "Ordered"'})
		}

};
*/


//-------SECTION: CHECK-OUT --------------------------

module.exports.checkOut = (req, res) =>{
	console.log(req.user.id); //id of the logged in user (with token)
	
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	let isUserCheckOut = User.findById(req.user.id).then(user =>{
		console.log(user);
	
		let checkOutAndTotal = {
	
			totalAmountDue: req.body.totalAmountdue
	
		}

		user.purchase.push(checkOutAndTotal)
		return res.send({message: 'Check-out Successful!'})
		.then(user => true)
		.catch(error => error.message)
	})

	


};





//-----------------GET ORDERS -------------------------------------------

module.exports.getOrders = (req, res) =>{
	User.findById(req.user.id)
	.then(result => res.send(result.purchase))
	.catch(error => res.send(error))



};

