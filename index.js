/*------------------------SESSION 37~41 ----------------------------
-------------- App: E-commerce API MVP Requirements-----------------

Notes:
A. Terminal
    Commands via terminal

        1. npm init -y
        2. npm i express nodemon mongoose bcryptjs jsonwebtoken cors
        3. touch .gitignore index.js
        4. mkdir controllers models routes
        5. npm run dev


B. package.json
    In package.json add "start" : "node index", and "dev" : "nodemon index" in "scipts"


    {
      "name": "s37-s41",
      "version": "1.0.0",
      "description": "",
      "main": "index.js",
      "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1",
        "start" : "node index", // <------------------- used since under dev
        "dev" : "nodemon index" // <------------------- used at the stage of  development
      },
      "keywords": [],
      "author": "",
      "license": "ISC",
      "dependencies": {
        "bcryptjs": "^2.4.3",
        "express": "^4.18.1",
        "jsonwebtoken": "^8.5.1",
        "mongoose": "^6.3.5",
        "nodemon": "^2.0.16"
      }
    }

C. .gitignore

    In .gitignore files, add "/node_modules"

*/

//---------------------END OF INITIAL SET-UP----------------------\\



//------------------SECTION - DEFENDENCIES--------------------------

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');


//-----------------------SECTION - SERVER-----------------------------
const app = express();
const port = process.env.PORT || 4000;

//---------------SECTION - Database Connection------------------------

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.s56h5.mongodb.net/capstone2?retryWrites=true&w=majority",

    // !important! additional code to avoid future error as MongoDB update versions
{
    useNewUrlParser: true,
    useUnifiedTopology: true
}
);

// this will create a notification if the db connection is sucessful or not
let db = mongoose.connection

// for error connection notification
db.on('error', console.error.bind(console, "Connection Error!"));

//for sucsessfull connection notification
db.once('open', () => console.log("Successfully connected to MongoDB"));





//---------------------SECTION - Midlewares ---------------------------

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


//--------------------SECTION - Group Routing-------------------------


const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);

const productRoutes = require('./routes/productRoutes');
app.use('/products', productRoutes);


//----------------------SECTION - Port Listener -------------------------


    app.listen(port, () => console.log(`Server is running at port ${port}`))
//-----------------------------------------------------------------------

