
// -------------------------SCHEMA--------------------------------

const mongoose = require('mongoose');

// Schema - a blueprint for our data/document 
const productSchema = new mongoose.Schema({

	productCode: {
		type: String,
		required: [true, "Product code is required"]
	},
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	size: {
		type: String,
		required: [true, "Size is required"]
	},
	price: {
		type: Number,
		required: [true, "Product price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			userId: {
				type: String,
				required: [true, "userID is required"]
				},
			dateEnrolled: {
				type: Date,
				default: new Date()
				},
			status: {
				type: String,
				default: "Added to Cart"
				}

		}
	]

})

// ---------------------MONGOOSE MODEL----------------------------
/*
	mongoose.model(<nameOfCollectionInAtlas>, <schedmaToFollow>)

*/

module.exports = mongoose.model("Product", productSchema);