
// -------------------------SCHEMA--------------------------------

const mongoose = require('mongoose');

// Schema - a blueprint for our data/document 
const userSchema = new mongoose.Schema({


	firstName: {
		type: String,
		required: [true, "Firstname is required"]
	},
	lastName: {
		type: String,
		required: [true, "Lastname is required"]
	},
	email: {
		type: String,
		required: [true, "Email address is required"]
	},
	houseNumberAndStreet: {
		type: String,
		required: [true, "House No. and Street Name is required"]
		},
	barangayAddr: {
		type: String,
		required: [true, "Barangay/Village is required"]
		},
	cityAddr: {
		type: String,
		required: [true, "City/Municipality is required"]
		},				
	provinceAddr: {
		type: String,
		required: [true, "District/Province name is required"]
		},
	regionAddr: {
		type: String,
		required: [true, "Region is required"]
	},

	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},	
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	purchase: [

		{
			productCode: {
				type: String,
				required: [true, "Product code is required"]
				},
			purchaseDate: {
				type: Date,
				default: new Date()
				},
			price: {
				type: Number,
				required: [true, "Price code is required"]
				},
/*			qty: {
				type: Number,
				required: [true, "Quantity is required"]
				},	*/
/*			preInkedColor: {
				type: Number,
				default: "Black"
				},	*/					
			status: {
				type: String,
				default: "Added to Cart"
				},
		}
	]

})

// ---------------------MONGOOSE MODEL----------------------------
/*
	mongoose.model(<nameOfCollectionInAtlas>, <schedmaToFollow>)

*/

module.exports = mongoose.model("User", userSchema);