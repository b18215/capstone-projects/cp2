//----------SECTION: Dependencies --------------------------------------
const express = require('express');
const router = express.Router();


//--------------- Router method - allows access to HTTP methods-----------

const productControllers = require('../controllers/productControllers');

//------------------------ Added from Auth ----------------------------

const auth = require('../auth');

//object destructuring from auth module
const { verify, verifyAdmin } = auth;


//------------create product route-----------------------------------------
router.post('/', verify, verifyAdmin, productControllers.registerProduct);

//--------------get all product route-------------------------------------
router.get('/', productControllers.getAllProducts);


//-----------------get single user route-----------------------------------
router.get('/getSingleProduct/:id', productControllers.getSingleProduct);



//--------------------updateProduct--------------------------------
router.put('/:id', verify, verifyAdmin, productControllers.updateProduct);



//--------------------/archive-product/:id--------------------------------
router.put('/archive/:id', verify, verifyAdmin, productControllers.archiveProduct);


//--------------------/activate=product/:id--------------------------------
router.put('/activate/:id', verify, verifyAdmin, productControllers.activateProduct);

//--------------------getActiveProducts-----------------------------------
router.get('/getActiveProducts', verify, productControllers.getActiveProducts);

//--------------------Get All Orders--------------------------------

router.get('/getAllOrders', verify, verifyAdmin, productControllers.getAllOrders);


module.exports = router;