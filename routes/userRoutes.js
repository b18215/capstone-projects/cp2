//----------SECTION: Dependencies --------------------------------------
const express = require('express');
const router = express.Router();

//--------------- Router method - allows access to HTTP methods-----------

const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

//object destructuring from auth module


const { verify, verifyAdmin } = auth;


//------------create user route-----------------------------------------
router.post('/', userControllers.registerUser);

//-----------get all users route----------------------------------------
router.get('/', userControllers.getAllUsers);

//------------------------Login User------------------------------------
router.post('/login', userControllers.loginUser);




//------------------getUserDetails--------------------------------------
router.get('/getUserDetails', verify, userControllers.getUserDetails);

//------------------------checkEmailExists------------------------------
router.post('/checkEmailExists', userControllers.checkEmailExists);

//--------------------updateUserDetails--------------------------------
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

/*//--------------------updateUserOrderStatus--------------------------------
router.put('/updateUserOrderStatus/:productId', verify, userControllers.updateUserOrderStatus);*/

//--------------------updateUserPassword--------------------------------
router.put('/updateUserPassword', verify, userControllers.updateUserPassword);

//-----------------updateAdmin---------------------------------------------
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);


//--------------------createOrder--------------------------------

router.post('/createOrder/:productCode', verify, userControllers.createOrder);

//--------------------Get Orders--------------------------------

router.get('/getOrders', verify, userControllers.getOrders);



//--------------------Check-Out--------------------------------

router.get('/checkOut', verify, userControllers.getOrders);



//--------------------------------------------------------------------------

module.exports = router;